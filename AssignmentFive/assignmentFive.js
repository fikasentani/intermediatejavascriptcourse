// TASK ONE
/* Code snippet 1 */
/* person; */ 

// code snippet one throws no error it is a variable, 
// however the variable must be declared wit either let if it shall be re assined or const if it shall not be reassigned

/* fix for code snippet one */ 
let person;

/* Code snippet 2
const user = {}; 
user.getName(); */

// snippet 2 throws a TypeError stating that user.getName is not a function
// to fix intern will have to create an method for getName and provide and implementation

/*fix for code snippet 2 and 3 */
const user = {
    getName: () => console.log("Snippet 2 --> get name invoked!"),
    fullName:   { firstName: "Fairhope" }
}; 
user.getName();

/* 
Code snippet 3
const user = {}; user.fullName.firstName = “Kaya”;
*/ 


// Code snippet 3 thows a TypeError complaining about firstName being undefined. Assuming that it uses the same "user object" in snippet 2.
// if not it will it will throw a SyntaxError saying user already has been declared.
// fix for this snippet is in code snippet 2
user.fullName.firstName = "Kaya";
console.log("snippet 3 ->", user);


/*Code snippet 4 
function setName(value){ const name = value;
}
setName("Kaya"); console.log(name);
*/

// Code snippet 4 throws a ReferenceError stating that name is not defined. 
// reason being name is not available outside of the setName scope
// to fix name will have to be defined

let name;
function setName(value){ 
    name = value;
}
setName("Kaya"); 
console.log("Snippet 4 -->", name);

// TASK TWO
console.log('------------------------------TASK TWO-------------------------');
// const getEvenNumbers = (numbers) => { const evenNumbers = [];
//     for(const i=0; i<numbers.length-1; i++;){
//     if(numbers % 2 = 0); { evenNumbers.push(i);
//     }
//     return evenNumbers; }
//     } 
//     getEvenNumbers([1,2,3,4,5,6,7,8]); 
//     console.log(name);

    /* There is a SyntaxError reslting in an unepected token on the for loop line, i++; is not supposed to end in a semi colon,
    * There is a logical Error caused by a syntax Error about an invalid left-hand side assignment by the if expression, 
    numbers % is being assigned (using one equal sign) to 2 instead of being compared to 2 with preferable the strict equality sign (===).
    * After the parenthesis of the same if satement there is a semi colon it does not break the code however its not common to see
    a semicolon after an 'if parenthesis' and it also causes unexpected/unpredicatable behaviour resulting in the block executing even when false
    * evenNumbers is declared (scoped) inside of the function making it unaccessible outside of the function scope should it need to be referenced outside of the function
    can either console log the function call since it returns a value which is evenNumbers array or can declare evenNumbers array globallly
    *  If the codesnippet in task two is linked to task one, then the console.log has a logical Error in that the worng variable is being printed
    name is being printed instead of evenNumbers array.
    * If task one and task two are two different code projects then the console log will return an Reference error complaining that name is not defined
    * Logical Error on the for line, i is declared as a constance variable menaing it should never be reassigned, swap const for let
    * Logical Error on numbers.length - 1 will miss the last item on the array not checking if its odd or even
    * LogicalEror on evenumbers.push(i) i is holding the index so only the index holding the even number will be added to the array, not the value of the even number in the array should be pushing numbers[i]
    * Logical Error on return EvenNumbers, the return statement terminates the loop on the first iteration. the return should happen outside of the for loop block 
    * Logical Error on numbers % 2 should be number[i] % 2 as numbers % 2 checks the whole array and numbers is of type NaN (not a number) as such the expression evaluates to false thus the block not executing nothing geting added onto the array
    * */
            /* Fix for equality for task two code snippet */ 
const getEvenNumbers = (numbers) => {
    const evenNumbers = [];
    for(let i = 0; i < numbers.length; i++) {
        if(numbers[i] % 2 === 0) {
            evenNumbers.push(numbers[i]);
        }
    }
    return evenNumbers;
}
const printEvenNumbers = getEvenNumbers([1,2,3,4,5,6,7,8,]);
console.log(printEvenNumbers);