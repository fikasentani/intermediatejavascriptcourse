const fetch = require("node-fetch");
const fetchCoinHelper = require("./fetchCoinDataHelper");
const chalk = require("chalk");

async function fetchFromCoinLore(coinID) {
  try {
    const coin = await fetch(
      `https://api.coinlore.net/api/ticker/?id=${coinID}`
    );
    const results = await coin.json();
    const coinLorePrice = parseFloat(results[0].price_usd);

    return coinLorePrice;
  } catch (error) {
    console.error(
      chalk.red("Sorry wat you typed is incorrect enter only btc, xrp or eth!")
    );
  }
}

async function fetchFromCryptonator(base) {
  try {
    const cryponator = await fetch(
      `https://api.cryptonator.com/api/ticker/${base}-usd`
    );
    const cryptonatorResult = await cryponator.json();
    const cryptonatorPrice = parseFloat(cryptonatorResult.ticker.price);
    return cryptonatorPrice;
  } catch (error) {
    console.error(
      chalk.red("Sorry wat you typed is incorrect enter only btc, xrp or eth!")
    );
  }
}

async function fetchFromCoinRanking(coinArg) {
  try {
    const coinRanking = await fetch(`https://api.coinranking.com/v2/coins`, {
      headers: {
        "Content-Type": "application/json",
        "x-access-token": "coinranking",
      },
    });
    const coinRankingResults = await coinRanking.json();
    const coinsArray = await coinRankingResults.data.coins;
    coin = coinsArray.find((coin) => coin.symbol === coinArg.toUpperCase());
    coinRankingPrice = parseFloat(coin.price);
    return coinRankingPrice;
  } catch (error) {
    console.error(
      chalk.red("Sorry wat you typed is incorrect enter only btc, xrp or eth!")
    );
  }
}

exports.enquirePrices = async (coin) => {
  try {
    const coinLore = await fetchFromCoinLore(
      fetchCoinHelper.fetchCoinHelper(coin)
    );
    const cryptonator = await fetchFromCryptonator(coin);
    const coinRanking = await fetchFromCoinRanking(coin);
    const averageprice = (await (coinLore + cryptonator + coinRanking)) / 3;
    return averageprice;
  } catch (error) {
    console.error(error);
  }
};
