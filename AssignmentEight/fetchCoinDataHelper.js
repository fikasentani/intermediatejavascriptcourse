exports.fetchCoinHelper = (symbol) => {
  let coinID;
  switch (symbol) {
    case "xrp":
      coinID = 58;
      break;

    case "btc":
      coinID = 90;
      break;

    case "eth":
      coinID = 80;
      break;
    default:
      coinID = 80;
  }

  return coinID;
};
