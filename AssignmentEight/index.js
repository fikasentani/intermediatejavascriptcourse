const chalk = require("chalk");
const enquirePrices = require("./fetchCoinData");

const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const firstQuestion = `Welcome to the Coin Tracker Tool. \n${chalk.bold.underline.italic(
  "What coin do you want to get the price of?"
)} \nEnter either (${chalk.yellow("BTC, ETH or XRP")}) `;

rl.question(firstQuestion, async function (coinName) {
  await enquirePrices
    .enquirePrices(coinName.toLowerCase().trim())
    .then((result) =>
      console.log(
        `the average price for ${coinName.toUpperCase()} is $${chalk.green.bold(
          result
        )}`
      )
    )
    .catch((err) =>
      console.error(
        chalk.orange("Please try again and enter only btc, xrp or eth!")
      )
    );
  rl.close();
});

rl.on("close", function () {
  console.log(chalk.blue("Thank you, good bye!"));
  process.exit(0);
});

//https://api.coinlore.net/api/tickers/
/*
*BTC id 90
*ETH id 80
*XRP id 58

*/
