const myPromise = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(5), 5000)
    })
}

async function myFunction() {
    const resolvedValue = myPromise();
    console.log(resolvedValue)
}

async function myFunctionAwait() {
    const resolvedValue = await myPromise();
    console.log(resolvedValue)
}

myFunction();
myFunctionAwait();