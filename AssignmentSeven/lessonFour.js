const myPromise1 = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(2), 2000)
    })
}

const myPromise2 = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(4))
    })
}

async function myFunction() {
    const value1 = await myPromise1();
    const value2 = await myPromise2();

    console.log(value1, value2)
    console.log('myFunction finished');
}

async function myConcurrentFunction() {
    const value1 = myPromise1();
    const value2 = myPromise2();

    console.log(await value1, await value2)
    console.log('myconcurrentfunction finished')
}

myFunction()
myConcurrentFunction()

async function myAllFunction() {
    const resolvedArray = await Promise.all([myPromise1(), myPromise2()]);

    resolvedArray.forEach((resolvedValue) => console.log(resolvedValue))
}

myAllFunction();