function isItAnObject(obj) {
    return new Promise((resolve, reject) => {
        if (obj instanceof Object) {
            resolve("It is an Object");
        } else {
            resolve("NOT OBJECT");
        }
    })
}

async function checkObj(obj) {
    const isObj = await isItAnObject(obj);
    console.log(isObj);
} 

checkObj({prop: "val"})
checkObj('{prop: "val"}')