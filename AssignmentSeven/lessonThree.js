async function myFunction() {
    try {
        const value = await asyncFunction()
    } catch(error) {
        console.log(error)
    }
}

myFunction()

async function myAsyncFunction() {
    const value = await myPromise()
}
myAsyncFunction()
.catch((val) => { console.log(val)})