async function myFunction() {
    return 'I am a promise'
}

myFunction().then((val) => console.log(val))

async function aPromise() {
    return new Promise((resolve, reject) => setTimeout(() => resolve("I take five seonds"), 5000))
}

async function aFunction() {
    const resolvedValue = await aPromise();
    console.log(resolvedValue)
}

aFunction()