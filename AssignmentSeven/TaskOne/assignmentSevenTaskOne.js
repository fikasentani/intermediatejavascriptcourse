//please run "npm install node-fetch"
const fetch = require('node-fetch');

const makeAsyncApiRequest = async(url) => {
    try {
        const response = await fetch(url);
        const results = await response.json()
        console.log('success--->',results.results[0]);
    } catch(err) {
        console.log('error---->>>>',err.message);
    }
}

makeAsyncApiRequest('https://randomuser.me/api/')
makeAsyncApiRequest('http://some-api.com')