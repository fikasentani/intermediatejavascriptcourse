const makeApiRequest = (url) => {
    const response = fetch(url)
    .then(response => {
        if(response.status === 200) {
            return response.json();
        } else {
            throw new Error(response.status);
        }
    })
    return response
}

makeApiRequest('https://randomuser.me/api/').then((res) => console.log(res)).catch((error) => console.log(error));
makeApiRequest('http://some-api.com').then((res) => console.log(res)).catch((error) => console.log(error));