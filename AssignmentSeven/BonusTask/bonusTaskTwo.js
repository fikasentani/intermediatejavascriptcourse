async function wait5Seconds() {
  await new Promise(resolve => setTimeout(resolve, 5000))
  return 'I waited 5 secods'
}

const callWait5SecondsWithAwait = async () => {
    const message = await wait5Seconds();
    console.log(message)
} 

function wait5SecondsNoAwait() {
    return new Promise((resolve, reject) => setTimeout(resolve('I waited 5 seconds'), 5000))
  }

callWait5SecondsWithAwait();
wait5SecondsNoAwait().then((res) => console.log(res))