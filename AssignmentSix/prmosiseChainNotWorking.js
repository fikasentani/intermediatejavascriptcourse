function resolvePromises(promises) {
    console.log('--->',promises)
    let arrayOfVals = [];

    const waitForValues = () => {
        return new Promise((resolve, reject) => {
       promises[0].then((a) => {arrayOfVals.push(a)
                 console.log(arrayOfVals)})
            promises[1].then((b) => {arrayOfVals.push(b)
                console.log(arrayOfVals)
            })
            promises[2].then((c) => {arrayOfVals.push(c)
                console.log(arrayOfVals)
            })
           return arrayOfVals
        })
    }

    // const populateArray = () => {
    //     promises[0].then((a) => {arrayOfVals.push(a)
    //         console.log(arrayOfVals)})
    //    promises[1].then((b) => {arrayOfVals.push(b)
    //        console.log(arrayOfVals)
    //    })
    //    promises[2].then((c) => {arrayOfVals.push(c)
    //        console.log(arrayOfVals)
    //    })

    //     return arrayOfVals
    // }
   
    // console.log('arraaayyyyyy', populateArray())
        // const waitFoValuesToResolve = new Promise((resolve, reject) => {
        //     promises[0].then((a) => arrayOfVals.push(a))
        //     promises[1].then((b) => arrayOfVals.push(b))
        //     promises[2].then((c) => arrayOfVals.push(c))
        //     return arrayOfVals
        // })

        
  return new Promise(async (resolve, reject) => {
    waitForValues().then((data) => console.log(data))
    //   await waitForValues().then((vals) => console.log(vals))
//    waitForValues().then((data) => console.log('--->',resolve(data)))
// waitFoValuesToResolve.then((data) => console.log('--->',resolve(data)))
    
  });
}

function waitSomeTime(value) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(value), Math.random() * 500);
  });
}

// resolvePromises([]).then((array) => {
//   console.log("should be []:", array);
// });

resolvePromises([waitSomeTime("a"), waitSomeTime("b"), waitSomeTime("c")]).then(
  (array) => {
    console.log("should be [a,b,c]", array);
  }
);

// resolvePromises([waitSomeTime(1), Promise.reject("error"), waitSomeTime(3)])
//   .then((array) => {
//     console.log("should never get here");
//   })
//   .catch((error) => {
//     if (error != "error") {
//       console.log("An error occured", error);
//     }
//   });

//   console.log('-->',Promise.reject('error rejection').then(e => console.log(e)))

// waitSomeTime('mpilo').then(mp => console.log('wait--> ' + mp))
