const firstPromise = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 5) + 1
    resolve(num);
});

const secondPromise = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 6) + 7
    resolve(num)
})

const thirdPromise = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 6) + 13
    resolve(num)
    // reject('third promise rejected')
})

const myPromises = Promise.all([firstPromise, secondPromise, thirdPromise]);

myPromises.then((values) => {
    console.log(values)
})
.catch((reason) => {
    console.log(reason);
})