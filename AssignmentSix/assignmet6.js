function resolvePromises(promises) {
  return new Promise(async(resolve, reject) => {
    let arrayOfVals = [];
    
    if (promises.length > 0) {
        try {
          await promises[0]
            .then((a) => arrayOfVals.push(a))
            .catch((e) => reject(e));
          await promises[1]
            .then((b, err) => {
                if (err) {
                    reject(err)
                } else {
                    arrayOfVals.push(b)
                }
            })
            .catch((error) => reject(error));
          await promises[2]
            .then((c) => arrayOfVals.push(c))
            .catch((e) => reject(e));
        } catch (error) {
          reject("it rejected");
        }

        if (arrayOfVals.length > 0) {
          resolve(arrayOfVals);
        } else {
          reject("no letters were sent through");
        }
      
    } else {
      resolve(arrayOfVals);
    }
  });
}

function waitSomeTime(value) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(value), Math.random() * 500);
  });
}

resolvePromises([]).then((array) => {
  console.log("should be []:", array);
});

resolvePromises([waitSomeTime("a"), waitSomeTime("b"), waitSomeTime("c")]).then(
  (array) => {
    console.log("should be [a,b,c]", array);
  }
);

resolvePromises([waitSomeTime(1), Promise.reject("error"), waitSomeTime(3)])
  .then((array) => {
    console.log("should never get here");
  })
  .catch((error) => {
    if (error != "error") {
      console.log("An error occured", error);
    }
  });

// waitSomeTime('mpilo').then(mp => console.log('wait--> ' + mp))
