const generateRandomNumber = () => Math.floor(Math.random() * 11);

const myPromise = new Promise((resolve, reject) => {
    const ranNumber = generateRandomNumber();
    console.log('the number is -->', ranNumber);
    setTimeout(()=> {
        if ( ranNumber % 2 === 0) {
            resolve('Resolved after some seconds with number ' + ranNumber);
        }  else {
            reject(`Rejected after some seconds with number ${ranNumber}`);
        }
    }, 3000);
})

myPromise.then(value => {
    console.log(value)
    return new Promise((resolve, reject) => {
        const ranNumber = generateRandomNumber();
        console.log('the number is -->', ranNumber);
        setTimeout(()=> {
            if ( ranNumber >= 5) {
                resolve('the second Resolved after some seconds with number ' + ranNumber);
            }  else {
                reject(` he second Rejected after some seconds with number ${ranNumber}`);
            }
        }, 5000);
    }).then(value => {
        console.log(value)
        return new Promise((resolve, reject) => {
            const ranNumber = generateRandomNumber();
            console.log('the number is -->', ranNumber);
            setTimeout(()=> {
                if ( ranNumber >= 5) {
                    resolve('the third Resolved after some seconds with number ' + ranNumber);
                }  else {
                    reject(` thhe third Rejected after some seconds with number ${ranNumber}`);
                }
            }, 2000);
        }).then((value)=> console.log(value)).catch((value) => console.log(value))
    }).catch(value => console.error(value))
}).catch(value => console.error(value))
generateRandomNumber()