/*
* ----- how to create a promise ---*
* Use the Promise class
* Instantiate it with a funtion called the executer

------------
* The executor is invoked as soon as the consructore runs
* Takes 2 function params 
    - resolve
    - reject
 they are pased into the executor   

 -------
 * Promises have 3 states
  - Pending
  - fulfilled
  - rejected

  * what really happens ?
  - the purpose of resolve is to convert the status from: 
                -  pending -------->> fulfilled / resolved

  - the purpose of rejected is to conert the status from:    
                - pening ----------->> rejected          
*/

const someCondition = true;
const executor = (resolve, reject) => {
    if(someCondition) resolve('Resoled')
    else reject('Rejected')
}

const myPromise = new Promise(executor)

