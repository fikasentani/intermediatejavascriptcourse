const generateRandomNumber = () => Math.floor(Math.random() * 11);
const ranNumber = generateRandomNumber();
const myPromise = new Promise((resolve, reject) => {
    // const ranNumber = generateRandomNumber();
    console.log('the number is -->', ranNumber);
    setTimeout(()=> {
        if ( ranNumber >= 5) {
            resolve('Resolved after some seconds with number ' + ranNumber);
        }  else {
            reject(`Rejected after some seconds with number ${ranNumber}`);
        }
    }, 5000);
})
console.log(myPromise);

// myPromise.then(value => console.log(value)).catch(value => console.error(value))
generateRandomNumber()

const myExecutor = (resolve, reject) => {
    setTimeout(() => {
        if (ranNumber <= 5) {
            resolve('executor resolved with ' + ranNumber)
        } else {
            reject(`Executor rejected with ${ranNumber}`)
        }
    }, 5000);
}

const myExecutorPromise = new Promise(myExecutor);