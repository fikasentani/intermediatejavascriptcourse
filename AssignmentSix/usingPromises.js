const myPromise = new Promise((resolve, reject) => {
    resolve('value of resolved');
})

const onFulfled = (value) => {
    console.log(value);
}

console.log(onFulfled);

myPromise.then(onFulfled);

const myOtherPromise = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 6) + 1
    if(num === 6) {
        resolve( 10 * num)
    } else {
        reject( 6  + num)
    }
})

const onResolved = (value) => {
    console.log('resolved--> ' + value)
}

const onRejected = (value) => {
    console.log('not 6---<<< ' + value)
}

myOtherPromise.then(onFulfled, onRejected);