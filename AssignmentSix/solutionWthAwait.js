function resolvePromises(promises) {
    console.log('--->',promises)
  return new Promise((resolve, reject) => {
    let arrayOfVals = [];
    if (promises[0], promises[1], promises[2]) {
      async function waitForResolve() {
        try {
          await promises[0]
            .then((a) => arrayOfVals.push(a))
            .catch((e) => reject(e));
          await promises[1]
            .then((b, error) => {
                try {
                    arrayOfVals.push(b) 
                } catch (e) {
                    console.log('de error-->',error)

                    reject('error in promise');
                }
                reject('rejecting')
                // b ? arrayOfVals.push(b) : reject('error in promise');
            }, () => { })
            .catch(() =>{ });
          await promises[2]
            .then((c) => arrayOfVals.push(c))
            .catch((e) => reject(e));
        } catch (error) {
          reject("it rejected");
        }

        if (arrayOfVals.length > 0 && arrayOfVals) {
          resolve(arrayOfVals);
        } else {
          reject("no letters were sent through");
        }
      }
      waitForResolve();
    } else {
      resolve(arrayOfVals);
    }
  });
}

function waitSomeTime(value) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(value), Math.random() * 500);
  });
}

// resolvePromises([]).then((array) => {
//   console.log("should be []:", array);
// });

// resolvePromises([waitSomeTime("a"), waitSomeTime("b"), waitSomeTime("c")]).then(
//   (array) => {
//     console.log("should be [a,b,c]", array);
//   }
// );

resolvePromises([waitSomeTime(1), Promise.reject("error"), waitSomeTime(3)])
  .then((array) => {
    console.log("should never get here");
  })
  .catch((error) => {
    if (error != "error") {
      console.log("An error occured", error);
    }
  });

  console.log('-->',Promise.reject('error rejection').then(e => console.log(e)))

// waitSomeTime('mpilo').then(mp => console.log('wait--> ' + mp))
