const promiseOne = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 6) + 1
    resolve(num)
});

const promiseTwo = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 6) + 1
    // reject('second promise rejected');
    resolve(num);
});

const thirdPromise = new Promise((resolve, rejected) => {
    const num = Math.floor(Math.random() * 6) + 13
    resolve(num)
});

const myPromises = Promise.all([promiseOne, promiseTwo, thirdPromise]);

myPromises.then((values) => {
    console.log(values);
})
.catch((reason) => {
    console.log(reason);
})