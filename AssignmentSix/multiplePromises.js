const myPromise = new Promise((resolve, reject) => {
    const num = Math.floor(Math.random() * 6) + 1
    if(num === 6) {
        resolve("I am resolved")
    } else {
        reject('I am rejected')
    }
})

myPromise.then((value) => {
    console.log(value)
})
.catch((reason) => {
    console.log(reason)
})