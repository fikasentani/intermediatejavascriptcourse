import Person from './Person';

class Manager extends Person {
    responsibilities = [];
    hoursWorked = [];
    hourlyRate;

    constructor( hoursWorked, hourlyRate) {

        this._hoursWorked = hoursWorked;
        this._hourlyRate = hourlyRate;
    }

    calculateAverageHoursWorked() {
        return this.addHoursWorked / this.hoursWorked.length;
    }

     addHoursWorked() {
        return this.hoursWorked.map(hour => hour =+ hour);
    }

    calculateRemuneration() {
        return this.addHoursWorked() * this.hourlyRate;
    }

    set hoursWorked(hoursWorked) {
        return this._hoursWorked = hoursWorked;
    }

    get hoursWorked() {
        return this._hoursWorked;
    }

    set hourlyRate(hourlyRate) {
        return this._hourlyRate = hourlyRate;
    }

    get hourlyRate() {
        return this.hourlyRate;
    }

}