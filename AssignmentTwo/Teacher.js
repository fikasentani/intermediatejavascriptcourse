import Person from './Person';

class Teacher extends Person  {
    _subject;
    _hoursWorked;
    _hourlyRate;

    constructor(subject, hoursWorked, hourlyRate) {
        this._subject = subject;
        this._hoursWorked = hoursWorked;
        this._hourlyRate = hourlyRate;
    }

    calculateAverageHoursWorked() {
        
    }

    addHoursWorked() {

    }

    calculateRemuneration() {

    }
    set subject(subject) {
        return this._subject = subject;
    }

    get subject() {
        return this._subject;
    }

    set hoursWorked(hoursWorked) {
        return this._hoursWorked = hoursWorked;
    }

    get hoursWorked() {
        return this._hoursWorked;
    }

    set hourlyRate(hourlyRate) {
        return this._hourlyRate = hourlyRate;
    }

    get hourlyRate() {
        return this.hourlyRate;
    }
    
}