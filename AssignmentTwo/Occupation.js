class Person {
    firstName;
    lastName;
    id;

    constructor(firstName, lastName, id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    getDetails() {
        return `${this.firstName} ${this.lastName} - ${this.id}`; 
    }

    set firstName(name) {
        return this.firstName = name;
    }

    get firstName() {
        return this.firstName;
    }

    set lastName(lastName) {
        return this.lastName = this.lastName;
    }

    get lastName() {
        return this.lastName;
    }

    set id(id) {
        return this.id = id;
    }

    get id() {
        return this.id;
    }
}
/////// ******************* STUDENT CLASS START ********* ///////////
class Student extends Person{
    
    subjects = [];
    grades = [];
    test = "test"
    constructor(firstName,lastName, id) {
        super(firstName, lastName, id) 
    }

    calculateAverageGrades() {
       return this.addGrades / this.grades.length;
    }

    addSubject(subject) {
        this.subjects.push(subject);
    }

    addGrades(grade) {
        return this.grades.map(grade => grade += grade);
    }

    setSubjects(subject) {
        this.addSubject(subject);
    }

    getSubjects() {
        return this.subjects;
    }

    setGrades(grade) {
        this.grades.push(grade);
    }

    getGrades() {
        return this.grades;
    }
    
}
/////// ******************* TEACHER CLASS START ********* ///////////
class Teacher extends Person  {
    subject;
    hoursWorked = [];
    hourlyRate;
    totalHoursWorked = 0;
    

    constructor(firstName, lastName, id, subject, hourlyRate) {
        super(firstName, lastName, id)
        this.subject = subject;
        this.hourlyRate = hourlyRate;  
    }

    calculateAverageHoursWorked() {
        return this.totalHoursWorked / this.hoursWorked.length;
    }

    addHoursWorked() {
         this.hoursWorked.forEach(hourWorked => {
            this.totalHoursWorked += hourWorked;
            return this.totalHoursWorked;
        });
    }

    calculateRemuneration() {
        const monthlySalary = this.hourlyRate * this.totalHoursWorked;
        return monthlySalary;
    }

    getDetails () {
        return `${this.firstName} ${this.lastName} - ${this.id}: teaches ${this.subject} earning R${this.calculateRemuneration()} this week
        after having worked an average of ${this.calculateAverageHoursWorked()} hours!`
    }
    set subject(subject) {
        return this.subject = subject;
    }

    get subject() {
        return this.subject;
    }

    setHoursWorked(...hoursWorked) {
        return this.hoursWorked = hoursWorked;
    }

    get hoursWorked() {
        return this.hoursWorked;
    }

    set hourlyRate(hourlyRate) {
        return this.hourlyRate = hourlyRate;
    }

    get hourlyRate() {
        return this.hourlyRate;
    }  
}
/////// ******************* MANAGER CLASS START ********* ///////////
class Manager extends Person {
    responsibilities = [];
    hoursWorked = [];
    hourlyRate = 0;
    totalRenumeration = 0;
    totalHoursWorked = 0;
    averageHoursWorked = 0;
    constructor(firstName, lastName, id, hourlyRate) {
        super(firstName, lastName, id)
        this.hourlyRate = hourlyRate;
    }
    calculateAverageHoursWorked() {
        this.averageHoursWorked = this.totalHoursWorked / this.hoursWorked.length;
    }
   

     addHoursWorked(hour) {
        this.hoursWorked.push(hour);
    }

    calculateRemuneration() {
        this.totalRenumeration =  this.setTotalHoursWorked() * this.hourlyRate;
    }

    setTotalHoursWorked() {
        let totalHours = 0;
        this.hoursWorked.forEach(hourWorked => {
            totalHours += hourWorked;
        })
         return this.totalHoursWorked = totalHours
    }

    addResponsibilities(...responsibilities) {
        this.responsibilities = responsibilities;
    }
    
    set hoursWorked(hoursWorked) {
        return this.hoursWorked = hoursWorked;
    }

    get hoursWorked() {
        return this.hoursWorked;
    }

    set hourlyRate(hourlyRate) {
        return this.hourlyRate = hourlyRate;
    }

    get hourlyRate() {
        return this.hourlyRate;
    }

}
/////// ******************* MANAGER CLASS END ********* ///////////
/////// ******************* STUDENT OBJECT ********* ///////////
console.log("/////// ******************* STUDENT OBJECT*********///////////");
const uiDeveloperSubjects = ['JavaScript', 'Swift', 'Java'];
const results = [100, 75, 60];
const mpilo = new Student("Mpilo", "Pilz", "1829304756");
uiDeveloperSubjects.forEach(subject => mpilo.setSubjects(subject));
results.forEach(result => mpilo.setGrades(result));
console.log(mpilo.getDetails());
console.log(mpilo);

/////// ******************* TEACHER OBJECT*********///////////
console.log("/////// ******************* TEACHER OBJECT*********///////////");

const thulani = new Teacher("Thulani", "Fikasen", "2019837465", "CSS", 500);
thulani.setHoursWorked(8, 5, 3, 4, 2, 10, 9, 7, 0);
thulani.addHoursWorked();
console.log(thulani.getDetails());
console.log(thulani);

/////// ******************* MANAGER OBJECT*********///////////
console.log("/////// ******************* MANAGER OBJECT*********///////////");
const nikita = new Manager("Nikita", "Kara", "5674839201", 1100);
nikita.addResponsibilities("Test Engineering", "Frontend Engineering", "Backend Engineering", "Mobile App Engineering", "DevOps", "Data Science", "Machine Learning");
nikita.addHoursWorked(3),
nikita.addHoursWorked(9),
nikita.addHoursWorked(4),
nikita.addHoursWorked(7),
nikita.addHoursWorked(2),
nikita.addHoursWorked(5),
nikita.addHoursWorked(10);
nikita.addHoursWorked(6);
nikita.addHoursWorked(8);
nikita.addHoursWorked(1);
nikita.setTotalHoursWorked();
nikita.calculateRemuneration();
nikita.calculateAverageHoursWorked();
console.log(nikita.getDetails());
console.log(nikita);