var students = [
  {
    name: "Kaya",
    email: "kaya@email.com",
    age: 16,
    favouriteSubjects: ["Mathematics", "Physical Science", "Information Technology"],
    plannedCareer: "Software Developer",
    hobbies: ["Bodybuilding", "Soccer"],
    hometown: {
      city: "Cape Town",
      province: "Western Cape",
    },
  },

  {
    name: "Lihle",
    email: "lihle@email.com",
    age: 17,
    favouriteSubjects: ["Geography", "Accounting", "Drama"],
    plannedCareer: "Software Tester",
    hobbies: ["Videogames", "Knitting"],
    hometown: {
      city: "Port Elisabeth",
      province: "Eastern Cape",
    },
  },

  {
    name: "Tshepiso",
    email: "tshepiso@email.com",
    age: 21,
    favouriteSubjects: ["Physics", "Art", "Chemistry"],
    plannedCareer: "UX Designer",
    hobbies: ["Videogames", "Diving"],
    hometown: {
      city: "Port Elisabeth",
      province: "Eastern Cape",
    },
  },

  {
    name: "Mathapelo",
    email: "mathapelo@email.com",
    age: 18,
    favouriteSubjects: ["Biology", "Business Studies", "Economics"],
    plannedCareer: "DevOps Engineer",
    hobbies: ["Surfing", "Swimming"],
    hometown: {
      city: "Durban",
      province: "KwaZulu Natal",
    },
  },
  {
    name: "Karabo",
    email: "karabo@email.com",
    age: 19,
    favouriteSubjects: ["Music", "English Literature", "Language Studies"],
    plannedCareer: "Business Analyst",
    hobbies: ["Diving", "Snorkeling"],
    hometown: {
      city: "Cape Town",
      province: "Western Cape",
    },
  },
  {
    name: "Mathapelo",
    email: "mathapelo2@email.com",
    age: 20,
    favouriteSubjects: ["Physical Education", "Art", "Mathemetics"],
    plannedCareer: "SQL Developer",
    hobbies: ["Surfing", "Swimming"],
    hometown: {
      city: "Durban",
      province: "KwaZulu Natal",
    },
  },
];
//Task 1
function printEmails(studentArr) {
  studentArr.forEach((student) => console.log(student.email));
}
// Task 2a
function printFavouriteSubjects(studentArr) {
  studentArr.forEach((student) => {
    student.favouriteSubjects.forEach((subject) =>
      console.log(`${student.name} likes ${subject}`)
    );
  });
}

//Task 2b instead of printing each subject in a new line creating a function that turns it into a phrase
function printFavoriteSubjectsDescriptive(studentArr) {
  studentArr.forEach((student) => {
    const studentFavoriteSubjects = student.favouriteSubjects;
    const getLastSubjectFromArray = studentFavoriteSubjects.slice(-1);
    const getAllSubjectsInAreMinusLastOne = studentFavoriteSubjects
      .slice(0, studentFavoriteSubjects.length - 1)
      .join(", ");
    const makeFavSubjetsPhrase = `${student.name}'s favorite subjects are ${getAllSubjectsInAreMinusLastOne} and ${getLastSubjectFromArray}`;
    console.log(makeFavSubjetsPhrase);
  });
}

//Task 3a - return first macth for city
function getStudentByCity(studentArr, cityName) {
  return studentArr.find((student) => {
    return student.hometown.city === cityName;
  });
}

//Task 3b - return all students from city
function getStudentsByCity(studentArr, cityName) {
  studentArr.find((student) => {
    if (student.hometown.city === cityName) {
      console.log(student);
    }
  });
}

//TASK 4
function getAllHobbies() {
  const arrayOfHobbies = students.map((student) => student.hobbies);
  const individualHobby = [];
  arrayOfHobbies.forEach((hobby) =>
    hobby.map((hobbyItem) => individualHobby.push(hobbyItem))
  );
//   console.log('debugging-->',individualHobby);
  //filter out and keep only unique hobbies
  return individualHobby.filter(
    (hobby, position) => individualHobby.indexOf(hobby) == position
  );
}

//Task 5
function hasPlannedCareer(career) {
  return students.some((student) => {
    return student.plannedCareer === career;
  });
}

//Task 6A
function getStudentByName(studentArr, studentName) {
  return studentArr.find((student) => {
    return student.name === studentName;
  });
}

//Task 6B
function getStudentsByName(studentArr, studentName) {
  studentArr.find((student) => {
    if (student.name === studentName) {
      console.log(student);
    }
  });
}

console.log(
  "-----------------------TASK 1 PRINT EMAILS--------------------------------------------"
);
console.log("");
printEmails(students);
console.log(
  "-----------------------TASK 2 PRINT FAVRITE SUBJECTS--------------------------------------------"
);
console.log("");
printFavouriteSubjects(students);
console.log("");
console.log(
  "----------------------- TASK 2 - SENTENCE VERSION OF FAVORITE SUBJECTS START--------------------------------------------"
);
console.log("");
printFavoriteSubjectsDescriptive(students);
console.log("");
console.log(
  "-----------------------SENTENCE VERSION OF FAVORITE SUBJECTS END--------------------------------------------"
);
console.log("");
console.log(
  "-----------------------TASK 3 SARCH STUDENT BY CITY--------------------------------------------"
);
console.log("");
console.log(getStudentByCity(students, "Cape Town"));
console.log(
  "----------------------- TASK 3 -OPTIONAL GET ALL STUDENTS THAT MATCH SEARCH START--------------------------------------------"
);
console.log("");
getStudentsByCity(students, "Cape Town");
console.log("");
console.log(
  "-----------------------OPTIONAL GET ALL STUDENTS THAT MATCH SEARCH END--------------------------------------------"
);

console.log(
  "-----------------------TASK 4 GET ALL HOBBIES--------------------------------------------"
);
console.log("");
console.log(getAllHobbies());
console.log("");
console.log(
  "-----------------------TASK 5 HAS PLANNED CARREERS--------------------------------------------"
);
console.log("");
console.log(hasPlannedCareer("Business Analyst"));
console.log(hasPlannedCareer("ProductOwner"));
console.log(hasPlannedCareer("Software Developer"));
console.log(hasPlannedCareer("Data Scientist"));
console.log(
  "-----------------------TASK 6 GET FIRST MATCH--------------------------------------------"
);
console.log("");
console.log(getStudentByName(students, "Mathapelo"));
console.log("");
console.log(
  "-----------------------OPTIONAL TASK 6 GET ALL MATCHES--------------------------------------------"
);
console.log("");
getStudentsByName(students, "Mathapelo");
