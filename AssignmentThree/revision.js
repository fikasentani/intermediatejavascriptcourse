const todoList = [
    {description: "Do laundry", done: true},
    {description: "Walk the dog", done: false},
    {description: "Buy groceries", done: true},
    {description: "Go to the gym", done: true},
    {description: "Do homework", done: false}
  ];
  const nextTask = todoList.find(task => {
    return task.done === false;
  })
  console.log(nextTask) // {description: ‘Walk the dog’, done: false},