const promptUser = require('prompt');
const writeToTxtFile = require('write');


    /* initiate the promt application */
  promptUser.start();

//  store the users details into variables
  promptUser.get(['name', 'surname', 'age', 'profession', 'lifeGoal' ], function (err, result) {
    /*
     Log the results for debuging.
    */
    console.log('Command-line input received:');
    console.log('  name: ' + result.name);
    console.log('  surname: ' + result.surname);
    console.log('  age: ' + result.age);
    console.log('  profession: ' + result.profession);
    console.log('  Life goal: ' + result.lifeGoal);

    /*write to a textfile*/

    writeToTxtFile(`${result.name}_${result.surname}.txt`, `
    Name: ${result.name},
    Surname: ${result.surname},
    Age: ${result.age},
    Profession: ${result.profession},
    Life Goal: ${result.lifeGoal}
    `)
  .then(() => {
    console.log(`open the text file named ${result.name}_${result.surname}.txt`);
  });
  });