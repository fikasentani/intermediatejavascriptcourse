// const promptUser = require('prompt');

const promptUser = require('prompt');
const writeToTxtFile = require('write');

function promptModule() {
  /* initiate the promt application */
  promptUser.start();

//  store te users details into variables
  promptUser.get(['name', 'surname', 'age', 'profession', 'lifeGoal' ], function (err, result) {
    /*
     Log the results.
    */
    console.log('Command-line input received:');
    console.log('  name: ' + result.name);
    console.log('  surname: ' + result.surname);
    console.log('  age: ' + result.age);
    console.log('  profession: ' + result.profession);
    console.log('  Life goal: ' + result.lifeGoal);

    const user = {
        name: result.name,
        surname: result.surname,
        age: result.age,
        profession: result.profession,
        lifeGoal: result.lifeGoal
    }
   
    return user;
    
  });
}
  

export default promptModule;
    